<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

header( 'Content-type: text/xml' );

class Btc_Deets {

	public function __construct( $tx_hash ) {

		$deets = $this->get_curl( 'http://btc.blockr.io/api/v1/tx/info/' . $tx_hash );

		$deets = json_decode( $deets, true );

		date_default_timezone_set( 'UTC' );

		$time = strtotime( $deets['data']['time_utc'] );

		$time_first = floor( $time / 86400 ) * 86400;
		$time_final = ceil(  $time / 86400 ) * 86400;

		$time_first = date( 'Y-m-d', $time_first );
		$time_final = date( 'Y-m-d', $time_final );

		$rx_address = $deets['data']['trade']['vouts'][0]['address'];

		$amount     = $deets['data']['trade']['vouts'][0]['amount'];

		$price = $this->get_price( $time_first, $time_final );

		$xml = new SimpleXMLElement("<deets/>");

		$xml->addChild( 'time', date( 'Y-m-d', $time ) );
		$xml->addChild( 'price', $price );
		$xml->addChild( 'rx', $rx_address );
		$xml->addChild( 'amount', $amount );

		echo $xml->asXML();

	}

	private function get_price( $time_first, $time_final ) {

		$price_dump = $this->get_curl( "http://api.coindesk.com/v1/bpi/historical/close.json?start={$time_first}&end={$time_final}"  );
		$price_dump = json_decode( $price_dump, true );

		$price = ( $price_dump['bpi'][$time_first] + $price_dump['bpi'][$time_final] ) / 2.0;

		return $price;
	}

	private function get_curl( $url ) {

		$curl = curl_init();

		curl_setopt( $curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36' );

		curl_setopt( $curl, CURLOPT_REFERER, $_SERVER['REQUEST_URI'] );
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt( $curl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true );

		curl_setopt( $curl, CURLOPT_COOKIESESSION, true );

		curl_setopt( $curl, CURLOPT_COOKIEJAR, 'cookie.txt' );
		curl_setopt( $curl, CURLOPT_COOKIEFILE, 'cookie.txt' );

		curl_setopt( $curl, CURLOPT_POST, false );

		curl_setopt( $curl, CURLOPT_URL,  $url );

		return curl_exec( $curl );

	}

}

$btc_deets = new Btc_Deets( $_REQUEST['tx'] );

/* EOF */
