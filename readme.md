## Synopsis

Rely on btc tx hash to fetch date and price info, then format it for integration
with Google Sheets.

## Code Example

=IMPORTXML(CONCATENATE("http://www.example.com/btc-deets/index.php?tx=", $C$10), "/deets/amount/text()")
=IMPORTXML(CONCATENATE("http://www.example.com/btc-deets/index.php?tx=", $C$10), "/deets/time/text()")
=IMPORTXML(CONCATENATE("http://www.example.com/btc-deets/index.php?tx=", $C$10), "/deets/price/text()")

## Installation

Install this code on your web server and then call its URL from Google Sheets.

## Contributors

@wikitopian

## License

[Nuremberg License](https://pastebin.com/yaG7WuNc)
